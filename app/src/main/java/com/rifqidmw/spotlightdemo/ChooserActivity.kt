package com.rifqidmw.spotlightdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class ChooserActivity : AppCompatActivity() {
    private val samples: Array<String> = arrayOf(
        SAMPLE_SPOTLIGHT_ON_ACTIVITY,
        SAMPLE_SPOTLIGHT_ON_FRAGMENT
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chooser)
        val listView = findViewById<ListView>(R.id.sample_list)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, samples)
        listView.adapter = adapter
        listView.setOnItemClickListener { _, _, position, _ ->
            when (samples[position]) {
                SAMPLE_SPOTLIGHT_ON_ACTIVITY -> {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
                SAMPLE_SPOTLIGHT_ON_FRAGMENT -> {
                    val intent = Intent(this, FragmentSampleActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    companion object {
        private const val SAMPLE_SPOTLIGHT_ON_ACTIVITY = "Spotlight on Activity"
        private const val SAMPLE_SPOTLIGHT_ON_FRAGMENT = "Spotlight on Fragment"
    }
}